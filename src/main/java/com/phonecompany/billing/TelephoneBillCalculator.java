package com.phonecompany.billing;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.Map.Entry;

public class TelephoneBillCalculator {
	private static final String DATE_TIME_FORMAT = "dd-MM-yyyy HH:mm:ss";
    private static final String BREAK_LINE = System.lineSeparator();
    private static final String COLUMN_SEPARATOR = ",";

    
	private static LocalDateTime parseDateTime(String dateTimeStr) {
		try {
	        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);
	        return LocalDateTime.parse(dateTimeStr, formatter);
		} catch(DateTimeParseException e) {
			return null;
		}
    }
	private static BigDecimal calculatePrice(String strLogs) {
		if (Objects.isNull(strLogs) || strLogs.trim().isEmpty()) {
		        return BigDecimal.ZERO;
		}
		// Convert to List Log
		List<PhoneLog> phoneLogs = new LinkedList<PhoneLog>();
        for (String row : strLogs.split(BREAK_LINE)) {
            String[] columns = row.split(COLUMN_SEPARATOR);
            if (columns.length == 3) {
            	String phoneNumber = columns[0];
            	LocalDateTime fromDateTime = parseDateTime(columns[1]);
            	LocalDateTime toDateTime = parseDateTime(columns[2]);
            	if(!Objects.isNull(phoneNumber) && !phoneNumber.trim().isEmpty()
            		&& !Objects.isNull(fromDateTime)
            		&& !Objects.isNull(toDateTime)
            	) {
            		 PhoneLog phoneLog = new PhoneLog(phoneNumber, fromDateTime, toDateTime);
                     phoneLogs.add(phoneLog);
            	}
               
            }
        }
        
        String mostCalledPhoneNumber = findMostCalledNumber(phoneLogs);
        BigDecimal amount = BigDecimal.ZERO;
        for (PhoneLog log : phoneLogs) {
        	if(!log.getPhone().equals(mostCalledPhoneNumber)) {
        		amount = amount.add(log.calculateByDuration());
        	}
        }
        return amount;
	}
	
	private static String findMostCalledNumber(List<PhoneLog> phoneLogs) {
		if (phoneLogs == null || phoneLogs.isEmpty()) {
			return null;
		}
		
		Map<String, Long> phoneInfos = phoneLogs.stream()
				.filter(Objects::nonNull)
				.map(PhoneLog::getPhone)
				.collect(Collectors.groupingBy(phone -> phone, Collectors.counting()));

		LinkedHashMap<String, Long> sortedPhoneInfos = sortByValue(phoneInfos);
		
		
		long maxValue = -1;
		String phone = null;
		for (Entry<String, Long> entry : sortedPhoneInfos.entrySet()) {
			if (maxValue == -1) {
				maxValue = entry.getValue();
				phone = entry.getKey();

			} else if (entry.getValue() < maxValue) {
				break;
			} else if (Long.parseLong(entry.getKey()) > Long.parseLong(phone)) {
				phone = entry.getKey();
			}
		}
		
		return phone;
	}
	
	private static LinkedHashMap<String, Long> sortByValue(final Map<String, Long> phoneInfos) {
        return phoneInfos.entrySet()
                .stream()
                .sorted((Map.Entry.<String, Long>comparingByValue().reversed()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
	}
	
	public static void main(String[] args) {
		String fakeInput = "420776562353,18-01-2020 08:59:20,18-01-2020 09:10:00\n420774577453,13-01-2020 18:10:15,13-01-2020 18:12:57\n420774577453,13-01-2020 18:10:15,13-01-2020 18:12:57";
		System.out.println(calculatePrice(fakeInput));
	}
}
