package com.phonecompany.billing;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Objects;

public class PhoneLog {
		
	public static final BigDecimal IN_HOUR_RATE = BigDecimal.ONE;
	public static final BigDecimal NORMAL_HOUR_RATE = BigDecimal.valueOf(0.5);
	public static final BigDecimal REDUCE_RATE = BigDecimal.valueOf(0.2);
	public static final int NUMBER_OF_MINUTE_BEFORE_REDUCE = 5;
	public static final LocalTime START_IN_HOUR = LocalTime.of(8, 00);
	public static final LocalTime END_IN_HOUR = LocalTime.of(16, 00);
	
	  private String phone;

	  private LocalDateTime timeStart;
	  
	  private LocalDateTime timeEnd;
	  
	  public PhoneLog(String phone, LocalDateTime timeStart, LocalDateTime timeEnd) {
		  this.phone = phone;
		  this.timeStart = timeStart;
		  this.timeEnd = timeEnd;
	  }
	  public void setPhone(String phone) {
	    this.phone = phone;
	  }
	  
	  public String getPhone() {
	    return this.phone;
	  }
	  
	  // behavior or method
	  public LocalDateTime getTimeStart() {
	    return this.timeStart;
	  }
	  
	  public void setTimeStart(LocalDateTime timeStart) {
	    this.timeStart = timeStart;
	  }
	  
	  // behavior or method
	  public LocalDateTime getTimeEnd() {
	    return this.timeEnd;
	  }
	  
	  public void setTimeEnd(LocalDateTime timeEnd) {
	    this.timeEnd = timeEnd;
	  }
	  
	  public BigDecimal calculateByDuration() {
	       if(this.timeEnd.isBefore(this.timeStart)) {
	           return BigDecimal.ZERO;
	       }
	       
	       int minutes = 1;
	       BigDecimal amount = BigDecimal.ZERO;
	       while(!this.timeStart.isAfter(this.timeEnd)) {
	           amount = amount.add(calculateByMinute(minutes++, this.timeStart));
	           this.timeStart = this.timeStart.plusMinutes(1).withSecond(0);
	       }
	       
	       return amount;
	   }
	 
	   private BigDecimal calculateByMinute(int minutes, LocalDateTime minuteCalculation) {
		   
		   BigDecimal rate = getRate(minuteCalculation);
	       if(minutes >= 5) {
	    	   rate = rate.subtract(REDUCE_RATE);
	       }
	       System.out.println(minuteCalculation);
	       System.out.println(minutes);
	       return rate;
	   }
	  
	   private BigDecimal getRate(LocalDateTime minuteCalculation) {
	       if(Objects.isNull(minuteCalculation)) {
	           return BigDecimal.ZERO;
	       }
	       LocalTime time = minuteCalculation.toLocalTime();
	      
	       if(!START_IN_HOUR.isAfter(time) && !time.isAfter(END_IN_HOUR)) {
	           return IN_HOUR_RATE;
	       }
	       return NORMAL_HOUR_RATE;
	   }

}

